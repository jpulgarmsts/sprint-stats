# Format to get sprint issues
# https://msts-eng.atlassian.net/rest/api/latest/search?jql=Sprint%20%3D%20%22Sorry%20Josh%22

# Experiment to read multiple sprint dates from customfield_10010
# a_sprint_startdate_list = []
# for a_sprint_startdate in data[0]["fields"]["customfield_10010"]:
#     a_sprint_startdate_position = a_sprint_startdate.find("startDate")
#     a_sprint_startdate_list.append(a_sprint_startdate[a_sprint_startdate_position+10:a_sprint_startdate_position+20])
# a_sprint_startdate_list.sort()
# a_sprint_startdate_list.reverse()
# SPRINT_START = a_sprint_startdate_list[0] +  "T08:00:00.000-0600"
# print(a_sprint_startdate_list)
# sys.exit()

import argparse, base64, json, os, sys, time, html, urllib.parse, urllib.request
from datetime import datetime, timedelta

from common import breakupWeekends, updt
import jiraapi

# MAIN PROGRAM:

# Define and parse command line arguments
parser = argparse.ArgumentParser()
parser.add_argument("SprintName", help="Name of the sprint in double quotes")
parser.add_argument("SprintStartDate", help="Beginning date of sprint YYYY-MM-DD")
parser.add_argument("-noweekends", help="Don't show weekends", action="store_true")
args = parser.parse_args()
SPRINT_START = args.SprintStartDate + "T08:00:00.000-0600"
if args.noweekends:
    NO_WEEKENDS = True
else:
    NO_WEEKENDS = False

# Set up JIRA API endpoints
download_url = "https://msts-eng.atlassian.net/rest/api/latest/search?maxResults=200&jql=Sprint=%22" + urllib.parse.quote(args.SprintName) + "%22"
print(download_url)
# If you ever need to hand pick issues for stats:
# download_url = "https://msts-eng.atlassian.net/rest/api/latest/search?startAt=0&maxResults=200&jql=project+%3D+PI+AND+key+in+(PI-2149%2C+PI-2299%2C+PI-2371%2C+PI-2380%2C+PI-4%2C+PI-2260%2C+PI-2370)"

# Get list of stories for the given "Sprint Name"
data = jiraapi.get_issues(download_url)

blockStart = {}
flagStart = {}

# Set up values needed for Javascript + HTML
PROJECT_NAME = data[0]["fields"]["project"]["name"]
final_text = ""
final_text_incident = ""
output_text = ""
incidentlist = []

# Check to make sure multiple teams don't have same sprint name
names_set = set()
for names_index in range(len(data)):
    names_set.add(data[names_index]["fields"]["project"]["name"])

# Set up progress bar
runs = len(data)
run_num = 0
print("Found " + str(runs) + " items to process in sprint \"" + args.SprintName + "\" for team(s): " + str(names_set))

# For each story in the given sprint...
for key in data:

    # Increment progress bar
    updt(runs, run_num + 1)
    run_num += 1

    # Ignore any sub-tasks
    if str(key["fields"].get("issuetype").get("name")) == "Sub-task":
        continue
    
    # Set up story/row title
    currentIssue = key["key"]
    currentPoints = str(key["fields"].get("customfield_10043"))
    if currentPoints == "None":
        currentPoints = "0"
    else:
        currentPoints = str(int(float(currentPoints)))
    
    # Handle the incidents
    if currentPoints == "0":
        incidentlist.append(html.escape(key["fields"]["updated"] + " " + key["fields"]["summary"].replace("'", "")) + " [<a href='https://msts-eng.atlassian.net/browse/" + key["key"] + "'>Link</a>]")
        continue

    currentSummary = "(" + currentPoints.zfill(2) + ") " + key["fields"]["summary"].replace("'", "")

    # Get story details for a given story id
    with jiraapi.get_issue_with_changelog(currentIssue) as f:
        issue_data = json.loads(f.read().decode('utf-8'))

    # If story was created after sprint started, don't use sprint start date
    if issue_data["fields"]["created"] > SPRINT_START:
        previous_time = issue_data["fields"]["created"]
    else:
        previous_time = SPRINT_START

    # Reverse the history so we see status changes from oldest to newest
    issue_key = issue_data["key"]
    issue_data = issue_data["changelog"]["histories"]
    issue_data.reverse()

    # Set up list that will hold all status changes
    mylist = []
    first_status_change = True

    READY_TO_WORK = 'To Do'
    
    # For each entry in the history...
    for issuekey in issue_data:

        # If we have a status change...
        for index in range(len(issuekey["items"])):
            
            # Keep track of Blocked label
            if issuekey["items"][index]["field"] == "labels":
                if "blocked" in issuekey["items"][index]["toString"].lower():
                    blockStart[issue_key] = issuekey["created"]
                if "blocked" in issuekey["items"][index]["fromString"].lower():
                    if "blocked" not in issuekey["items"][index]["toString"].lower() == "":
                        if issue_key in blockStart:
                            mylist.append(currentSummary + "#|#" + str(blockStart[issue_key]) + "#|#" + str(issuekey["created"]) + "#|#Blocked")

            # Keep track of the story being flagged for impediment (the new Blocked?)
            if issuekey["items"][index]["field"] == "Flagged":
                fromString = issuekey["items"][index]["fromString"]
                toString   = issuekey["items"][index]["toString"]

                if fromString in (None, "") and toString not in (None, ""):
                    flagStart[issue_key] = issuekey["created"]
                elif fromString not in (None, "") and toString in (None, "") and issue_key in flagStart:
                    mylist.append(currentSummary + "#|#" + str(flagStart[issue_key]) + "#|#" + str(issuekey["created"]) + "#|#Flagged")

            if issuekey["items"][index]["field"] == "status":

                # If this is the first status change, add the To Do status
                if (first_status_change):
                    if issuekey["created"] < SPRINT_START:
                        issuekey["created"] = SPRINT_START
                    if previous_time > SPRINT_START:
                        previous_time = SPRINT_START
                    
                    if (NO_WEEKENDS):
                        last_start_time = breakupWeekends(mylist, currentSummary, str(previous_time), str(issuekey["created"]), READY_TO_WORK)
                    else:
                        mylist.append(currentSummary + "#|#" + str(previous_time) + "#|#" + str(issuekey["created"]) + "#|#" + "To Do")
                        last_start_time = str(issuekey["created"])
                    
                    first_status_change = False
                    last_status = issuekey["items"][index]["toString"]

                    # Generate Done status
                    if issuekey["items"][index]["toString"] == "Done":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Done")

                    # Generate Closed status
                    if issuekey["items"][index]["toString"] == "Closed":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Closed")    

                    # Generate Resolved status
                    if issuekey["items"][index]["toString"] == "Resolved":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Resolved") 

                    # Generate Accepted status
                    if issuekey["items"][index]["toString"] == "Accepted":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Accepted") 

                # Else it's not the first status change  
                else:
                    if last_start_time < SPRINT_START:
                        last_start_time = SPRINT_START
                    if issuekey["created"] < SPRINT_START:
                        issuekey["created"] = SPRINT_START
                    
                    if (NO_WEEKENDS):
                        last_start_time = breakupWeekends(mylist, currentSummary, str(last_start_time), str(issuekey["created"]), last_status)
                    else:
                        mylist.append(currentSummary + "#|#" + str(last_start_time) + "#|#" + str(issuekey["created"]) + "#|#" + last_status)
                        last_start_time = str(issuekey["created"])
                    
                    previous_time = issuekey["created"]
                    last_status = issuekey["items"][index]["toString"]
                    
                    # Generate Done status
                    if issuekey["items"][index]["toString"] == "Done":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Done")

                    # Generate Closed status
                    if issuekey["items"][index]["toString"] == "Closed":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Closed")

                    # Generate Resolved status
                    if issuekey["items"][index]["toString"] == "Resolved":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Resolved")

                    # Generate Accepted status
                    if issuekey["items"][index]["toString"] == "Accepted":
                        mylist.append(currentSummary + "#|#" + str(issuekey["created"]) + "#|#" + str(issuekey["created"]) + "#|#Accepted")

    # Save the text that will go into Javascript file
    for x in range(len(mylist)): 
        output_issue, output_start, output_end, output_status = mylist[x].split("#|#")
        if output_status == READY_TO_WORK:
            continue
        output_text = "["
        output_text += "'" + output_issue + "', "
        output_text += "'" + output_status + "', "
        output_text += "moment(\"" + output_start + "\", \"YYYY-MM-DDTHH:mm:ss.SSS\").toDate(), "
        output_text += "moment(\"" + output_end + "\", \"YYYY-MM-DDTHH:mm:ss.SSS\").toDate()"
        output_text += "]"
        output_text += ","
        final_text += output_text + '\n'

incidentlist.sort()
for x in range(len(incidentlist)): 
    final_text_incident += '<li>' + incidentlist[x][29:] + '</li>'

# Generate the Javascript file
output_js_file = open("sprint_output.js","w")
output_js_file.write("var sprint_output = [" + final_text[:-1] + "];\n")
output_js_file.write("var start_date = moment(\"" + SPRINT_START + "\", \"YYYY-MM-DDTHH:mm:ss.SSS\").toDate();\n")
output_js_file.write("var skip_weekends = " + (NO_WEEKENDS and "true" or "false") +";\n")
# output_js_file.write("document.getElementById(\"sprint-title\").innerHTML = \"" + PROJECT_NAME + ": " + sys.argv[1] + "\";\n")
output_js_file.write("document.getElementById(\"sprint-title\").innerHTML = \"" + sys.argv[1] + "\";\n")
output_js_file.write("document.getElementById(\"incidents\").innerHTML = \"" + final_text_incident[:-1] + "\";\n")
output_js_file.close()
