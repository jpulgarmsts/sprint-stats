import base64, json, os, urllib.parse, urllib.request
from functools import cache

# Make an authenticated request to JIRA
def request(url):
    req = urllib.request.Request(url)
    req.add_header('Authorization', authorization_header())
    return urllib.request.urlopen(req)

# Get JIRA API credentials from environment variable and cache them in request header form
@cache
def authorization_header():
    username = os.environ['JIRA_EMAIL']
    password = os.environ['JIRA_KEY']
    credentials = ('%s:%s' % (username, password))
    encoded_credentials = base64.b64encode(credentials.encode('ascii'))
    return 'Basic %s' % encoded_credentials.decode("ascii")

def get_issues(start_url):
    data = []
    offset = '0'

    # TODO: this could be a loop

    with request(start_url + '&startAt=' + offset) as f:
        jsondata = json.loads(f.read().decode('utf-8'))
        data.extend(jsondata["issues"])

    if (jsondata["total"] > 100):
        # download_url = download_url.replace("startAt=0", "startAt=100")
        offset = '100'
        with request(start_url + '&startAt=' + offset) as f:
            jsondata = json.loads(f.read().decode('utf-8'))
            data.extend(jsondata["issues"])

    if (jsondata["total"] > 200):
        # download_url = download_url.replace("startAt=100", "startAt=200")
        offset = '200'
        with request(start_url + '&startAt=' + offset) as f:
            jsondata = json.loads(f.read().decode('utf-8'))
            data.extend(jsondata["issues"])

    return data

issue_root_url = "https://msts-eng.atlassian.net/rest/api/latest/issue/"
issue_params = "?expand=changelog"

def get_issue_with_changelog(currentIssue):
    return request(issue_root_url + currentIssue + issue_params)
