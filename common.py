import sys
from datetime import datetime, timedelta
from functools import cache

# HELPER FUNCTIONS:

# Given a single date range (startDate -> endDate), break it up into multiple
# date ranges (if necessary) so that weekends are not included.
# Fridays are cut off at 11:59pm. Mondays start at 6:00am
def breakupWeekends(listOfRanges, summary, startDate, endDate, status):
    originalStartDate = datetime.strptime(startDate, '%Y-%m-%dT%H:%M:%S.%f%z')
    originalEndDate = datetime.strptime(endDate, '%Y-%m-%dT%H:%M:%S.%f%z')
    loopDate = datetime.strptime(startDate, '%Y-%m-%dT%H:%M:%S.%f%z')
    weekendStartDate = False;
    inWeekend = False
    startedBreak = False

    the_day_after_originalEndDate = (originalEndDate + timedelta(days=1)).day

    while (loopDate.day != the_day_after_originalEndDate):
        if (loopDate.weekday() > 4):
            if not inWeekend:
                inWeekend = True
                startedBreak = True
                tempDate = loopDate
                tempDate += timedelta(days=-1)
                tempDate = tempDate.replace(hour=23, minute=59, second=59)
                weekendStartDate = tempDate
                listOfRanges.append(summary + "#|#" + str(originalStartDate.strftime('%Y-%m-%dT%H:%M:%S.%f%z')) + "#|#" + str(tempDate.strftime('%Y-%m-%dT%H:%M:%S.%f%z')) + "#|#" + status)
        else:
            inWeekend = False
            if startedBreak:
                startedBreak = False
                tempDate = loopDate
                tempDate = tempDate.replace(hour=6, minute=0, second=0)
                listOfRanges.append(summary + "#|#" + str(weekendStartDate.strftime('%Y-%m-%dT%H:%M:%S.%f%z')) + "#|#" + str(tempDate.strftime('%Y-%m-%dT%H:%M:%S.%f%z')) + "#|#" + 'WEEKEND')
                originalStartDate = tempDate
        loopDate += timedelta(days=1)

    listOfRanges.append(summary + "#|#" + str(originalStartDate.strftime('%Y-%m-%dT%H:%M:%S.%f%z')) + "#|#" + str(originalEndDate.strftime('%Y-%m-%dT%H:%M:%S.%f%z')) + "#|#" + status)

    return str(originalEndDate.strftime('%Y-%m-%dT%H:%M:%S.%f%z'))

# Used to implement the progress bar
def updt(total, progress):
    barLength, status = 50, ""
    progress = float(progress) / float(total)
    if progress >= 1.:
        progress, status = 1, "\r\n"
    block = int(round(barLength * progress))
    text = "\rProcessing stories... [{}] {:.0f}% {}".format(
        "#" * block + "-" * (barLength - block), round(progress * 100, 0),
        status)
    sys.stdout.write(text)
    sys.stdout.flush()
